# Mobwa::CreateAccountRequest

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **name** | **String** |  | [optional] |

## Example

```ruby
require 'mobwa'

instance = Mobwa::CreateAccountRequest.new(
  name: null
)
```

