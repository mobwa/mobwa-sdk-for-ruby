# Mobwa::UpdateAccountRequest

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **name** | **String** |  | [optional] |

## Example

```ruby
require 'mobwa'

instance = Mobwa::UpdateAccountRequest.new(
  name: null
)
```

