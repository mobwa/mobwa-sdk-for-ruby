# Mobwa::AccountsApi

All URIs are relative to *http://localhost*

| Method | HTTP request | Description |
| ------ | ------------ | ----------- |
| [**create_account**](AccountsApi.md#create_account) | **POST** /accounts | Create an account |
| [**get_account**](AccountsApi.md#get_account) | **GET** /accounts/{accountId} | Get account information |
| [**list_account**](AccountsApi.md#list_account) | **GET** /accounts | List all accounts |
| [**recharge_account**](AccountsApi.md#recharge_account) | **POST** /accounts/{accountId}/recharge | Recharge the account |
| [**update_account**](AccountsApi.md#update_account) | **PUT** /accounts/{accountId} | Update account information |
| [**withdraw_money**](AccountsApi.md#withdraw_money) | **POST** /accounts/{accountId}/withdraw | Withdraw money from the account |


## create_account

> <Array<Transfer>> create_account(opts)

Create an account

### Examples

```ruby
require 'time'
require 'mobwa'
# setup authorization
Mobwa.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Mobwa::AccountsApi.new
opts = {
  create_account_request: Mobwa::CreateAccountRequest.new # CreateAccountRequest | 
}

begin
  # Create an account
  result = api_instance.create_account(opts)
  p result
rescue Mobwa::ApiError => e
  puts "Error when calling AccountsApi->create_account: #{e}"
end
```

#### Using the create_account_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(<Array<Transfer>>, Integer, Hash)> create_account_with_http_info(opts)

```ruby
begin
  # Create an account
  data, status_code, headers = api_instance.create_account_with_http_info(opts)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => <Array<Transfer>>
rescue Mobwa::ApiError => e
  puts "Error when calling AccountsApi->create_account_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **create_account_request** | [**CreateAccountRequest**](CreateAccountRequest.md) |  | [optional] |

### Return type

[**Array&lt;Transfer&gt;**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## get_account

> <Account> get_account(account_id)

Get account information

### Examples

```ruby
require 'time'
require 'mobwa'
# setup authorization
Mobwa.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Mobwa::AccountsApi.new
account_id = 'account_id_example' # String | The id of the account

begin
  # Get account information
  result = api_instance.get_account(account_id)
  p result
rescue Mobwa::ApiError => e
  puts "Error when calling AccountsApi->get_account: #{e}"
end
```

#### Using the get_account_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(<Account>, Integer, Hash)> get_account_with_http_info(account_id)

```ruby
begin
  # Get account information
  data, status_code, headers = api_instance.get_account_with_http_info(account_id)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => <Account>
rescue Mobwa::ApiError => e
  puts "Error when calling AccountsApi->get_account_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **account_id** | **String** | The id of the account |  |

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## list_account

> <Array<Account>> list_account

List all accounts

### Examples

```ruby
require 'time'
require 'mobwa'
# setup authorization
Mobwa.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Mobwa::AccountsApi.new

begin
  # List all accounts
  result = api_instance.list_account
  p result
rescue Mobwa::ApiError => e
  puts "Error when calling AccountsApi->list_account: #{e}"
end
```

#### Using the list_account_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(<Array<Account>>, Integer, Hash)> list_account_with_http_info

```ruby
begin
  # List all accounts
  data, status_code, headers = api_instance.list_account_with_http_info
  p status_code # => 2xx
  p headers # => { ... }
  p data # => <Array<Account>>
rescue Mobwa::ApiError => e
  puts "Error when calling AccountsApi->list_account_with_http_info: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**Array&lt;Account&gt;**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## recharge_account

> <Account> recharge_account(account_id, opts)

Recharge the account

### Examples

```ruby
require 'time'
require 'mobwa'
# setup authorization
Mobwa.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Mobwa::AccountsApi.new
account_id = 'account_id_example' # String | The id of the account
opts = {
  recharge_account_request: Mobwa::RechargeAccountRequest.new # RechargeAccountRequest | 
}

begin
  # Recharge the account
  result = api_instance.recharge_account(account_id, opts)
  p result
rescue Mobwa::ApiError => e
  puts "Error when calling AccountsApi->recharge_account: #{e}"
end
```

#### Using the recharge_account_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(<Account>, Integer, Hash)> recharge_account_with_http_info(account_id, opts)

```ruby
begin
  # Recharge the account
  data, status_code, headers = api_instance.recharge_account_with_http_info(account_id, opts)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => <Account>
rescue Mobwa::ApiError => e
  puts "Error when calling AccountsApi->recharge_account_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **account_id** | **String** | The id of the account |  |
| **recharge_account_request** | [**RechargeAccountRequest**](RechargeAccountRequest.md) |  | [optional] |

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## update_account

> <Account> update_account(account_id, opts)

Update account information

### Examples

```ruby
require 'time'
require 'mobwa'
# setup authorization
Mobwa.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Mobwa::AccountsApi.new
account_id = 'account_id_example' # String | The id of the account
opts = {
  update_account_request: Mobwa::UpdateAccountRequest.new # UpdateAccountRequest | 
}

begin
  # Update account information
  result = api_instance.update_account(account_id, opts)
  p result
rescue Mobwa::ApiError => e
  puts "Error when calling AccountsApi->update_account: #{e}"
end
```

#### Using the update_account_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(<Account>, Integer, Hash)> update_account_with_http_info(account_id, opts)

```ruby
begin
  # Update account information
  data, status_code, headers = api_instance.update_account_with_http_info(account_id, opts)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => <Account>
rescue Mobwa::ApiError => e
  puts "Error when calling AccountsApi->update_account_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **account_id** | **String** | The id of the account |  |
| **update_account_request** | [**UpdateAccountRequest**](UpdateAccountRequest.md) |  | [optional] |

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## withdraw_money

> <Account> withdraw_money(account_id, opts)

Withdraw money from the account

### Examples

```ruby
require 'time'
require 'mobwa'
# setup authorization
Mobwa.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Mobwa::AccountsApi.new
account_id = 'account_id_example' # String | The id of the account
opts = {
  body: 3.56 # RechargeAccountRequest | 
}

begin
  # Withdraw money from the account
  result = api_instance.withdraw_money(account_id, opts)
  p result
rescue Mobwa::ApiError => e
  puts "Error when calling AccountsApi->withdraw_money: #{e}"
end
```

#### Using the withdraw_money_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(<Account>, Integer, Hash)> withdraw_money_with_http_info(account_id, opts)

```ruby
begin
  # Withdraw money from the account
  data, status_code, headers = api_instance.withdraw_money_with_http_info(account_id, opts)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => <Account>
rescue Mobwa::ApiError => e
  puts "Error when calling AccountsApi->withdraw_money_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **account_id** | **String** | The id of the account |  |
| **body** | **RechargeAccountRequest** |  | [optional] |

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

