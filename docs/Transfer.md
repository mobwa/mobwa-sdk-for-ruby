# Mobwa::Transfer

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **id** | **String** |  | [optional] |
| **message** | **String** |  | [optional] |
| **amount** | **Integer** |  | [optional] |
| **currency** | **String** |  | [optional] |
| **created_at** | **String** |  | [optional] |
| **updated_at** | **String** |  | [optional] |
| **status** | **String** |  | [optional] |
| **auto_complete** | **Boolean** |  | [optional] |
| **source** | [**TransferSource**](TransferSource.md) |  | [optional] |
| **destination** | [**TransferSource**](TransferSource.md) |  | [optional] |

## Example

```ruby
require 'mobwa'

instance = Mobwa::Transfer.new(
  id: null,
  message: null,
  amount: null,
  currency: null,
  created_at: null,
  updated_at: null,
  status: null,
  auto_complete: null,
  source: null,
  destination: null
)
```

