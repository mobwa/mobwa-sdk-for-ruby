# Mobwa::TransfersApi

All URIs are relative to *http://localhost*

| Method | HTTP request | Description |
| ------ | ------------ | ----------- |
| [**complete_transfer**](TransfersApi.md#complete_transfer) | **POST** /accounts/{accountId}/transfers/{transferId}/complete | Complete a transfer |
| [**create_transfer**](TransfersApi.md#create_transfer) | **POST** /accounts/{accountId}/transfers | Create a transfer |
| [**delete_transfer**](TransfersApi.md#delete_transfer) | **DELETE** /accounts/{accountId}/transfers/{transferId} | Delete a transfer |
| [**get_transfer**](TransfersApi.md#get_transfer) | **GET** /accounts/{accountId}/transfers/{transferId} | Retrieve information for a specific transfer |
| [**list_transfers**](TransfersApi.md#list_transfers) | **GET** /accounts/{accountId}/transfers | List all transfers related to the account |
| [**update_transfer**](TransfersApi.md#update_transfer) | **PUT** /accounts/{accountId}/transfers/{transferId} | Change transfer information |


## complete_transfer

> <Transfer> complete_transfer(account_id, transfer_id)

Complete a transfer

### Examples

```ruby
require 'time'
require 'mobwa'
# setup authorization
Mobwa.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Mobwa::TransfersApi.new
account_id = 'account_id_example' # String | The id of the account
transfer_id = 'transfer_id_example' # String | The id of the transfer to operate on

begin
  # Complete a transfer
  result = api_instance.complete_transfer(account_id, transfer_id)
  p result
rescue Mobwa::ApiError => e
  puts "Error when calling TransfersApi->complete_transfer: #{e}"
end
```

#### Using the complete_transfer_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(<Transfer>, Integer, Hash)> complete_transfer_with_http_info(account_id, transfer_id)

```ruby
begin
  # Complete a transfer
  data, status_code, headers = api_instance.complete_transfer_with_http_info(account_id, transfer_id)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => <Transfer>
rescue Mobwa::ApiError => e
  puts "Error when calling TransfersApi->complete_transfer_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **account_id** | **String** | The id of the account |  |
| **transfer_id** | **String** | The id of the transfer to operate on |  |

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## create_transfer

> <Transfer> create_transfer(account_id, opts)

Create a transfer

### Examples

```ruby
require 'time'
require 'mobwa'
# setup authorization
Mobwa.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Mobwa::TransfersApi.new
account_id = 'account_id_example' # String | The id of the account
opts = {
  create_transfer_request: Mobwa::CreateTransferRequest.new # CreateTransferRequest | 
}

begin
  # Create a transfer
  result = api_instance.create_transfer(account_id, opts)
  p result
rescue Mobwa::ApiError => e
  puts "Error when calling TransfersApi->create_transfer: #{e}"
end
```

#### Using the create_transfer_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(<Transfer>, Integer, Hash)> create_transfer_with_http_info(account_id, opts)

```ruby
begin
  # Create a transfer
  data, status_code, headers = api_instance.create_transfer_with_http_info(account_id, opts)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => <Transfer>
rescue Mobwa::ApiError => e
  puts "Error when calling TransfersApi->create_transfer_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **account_id** | **String** | The id of the account |  |
| **create_transfer_request** | [**CreateTransferRequest**](CreateTransferRequest.md) |  | [optional] |

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## delete_transfer

> <Transfer> delete_transfer(account_id, transfer_id)

Delete a transfer

### Examples

```ruby
require 'time'
require 'mobwa'
# setup authorization
Mobwa.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Mobwa::TransfersApi.new
account_id = 'account_id_example' # String | The id of the account
transfer_id = 'transfer_id_example' # String | The id of the transfers to operate on

begin
  # Delete a transfer
  result = api_instance.delete_transfer(account_id, transfer_id)
  p result
rescue Mobwa::ApiError => e
  puts "Error when calling TransfersApi->delete_transfer: #{e}"
end
```

#### Using the delete_transfer_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(<Transfer>, Integer, Hash)> delete_transfer_with_http_info(account_id, transfer_id)

```ruby
begin
  # Delete a transfer
  data, status_code, headers = api_instance.delete_transfer_with_http_info(account_id, transfer_id)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => <Transfer>
rescue Mobwa::ApiError => e
  puts "Error when calling TransfersApi->delete_transfer_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **account_id** | **String** | The id of the account |  |
| **transfer_id** | **String** | The id of the transfers to operate on |  |

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## get_transfer

> <Transfer> get_transfer(account_id, transfer_id)

Retrieve information for a specific transfer

### Examples

```ruby
require 'time'
require 'mobwa'
# setup authorization
Mobwa.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Mobwa::TransfersApi.new
account_id = 'account_id_example' # String | The id of the account
transfer_id = 'transfer_id_example' # String | The id of the transfers to operate on

begin
  # Retrieve information for a specific transfer
  result = api_instance.get_transfer(account_id, transfer_id)
  p result
rescue Mobwa::ApiError => e
  puts "Error when calling TransfersApi->get_transfer: #{e}"
end
```

#### Using the get_transfer_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(<Transfer>, Integer, Hash)> get_transfer_with_http_info(account_id, transfer_id)

```ruby
begin
  # Retrieve information for a specific transfer
  data, status_code, headers = api_instance.get_transfer_with_http_info(account_id, transfer_id)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => <Transfer>
rescue Mobwa::ApiError => e
  puts "Error when calling TransfersApi->get_transfer_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **account_id** | **String** | The id of the account |  |
| **transfer_id** | **String** | The id of the transfers to operate on |  |

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## list_transfers

> <Array<Transfer>> list_transfers(account_id)

List all transfers related to the account

### Examples

```ruby
require 'time'
require 'mobwa'
# setup authorization
Mobwa.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Mobwa::TransfersApi.new
account_id = 'account_id_example' # String | The id of the account

begin
  # List all transfers related to the account
  result = api_instance.list_transfers(account_id)
  p result
rescue Mobwa::ApiError => e
  puts "Error when calling TransfersApi->list_transfers: #{e}"
end
```

#### Using the list_transfers_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(<Array<Transfer>>, Integer, Hash)> list_transfers_with_http_info(account_id)

```ruby
begin
  # List all transfers related to the account
  data, status_code, headers = api_instance.list_transfers_with_http_info(account_id)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => <Array<Transfer>>
rescue Mobwa::ApiError => e
  puts "Error when calling TransfersApi->list_transfers_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **account_id** | **String** | The id of the account |  |

### Return type

[**Array&lt;Transfer&gt;**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## update_transfer

> <Transfer> update_transfer(account_id, transfer_id, opts)

Change transfer information

### Examples

```ruby
require 'time'
require 'mobwa'
# setup authorization
Mobwa.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Mobwa::TransfersApi.new
account_id = 'account_id_example' # String | The id of the account
transfer_id = 'transfer_id_example' # String | The id of the transfers to operate on
opts = {
  update_transfer_request: Mobwa::UpdateTransferRequest.new # UpdateTransferRequest | 
}

begin
  # Change transfer information
  result = api_instance.update_transfer(account_id, transfer_id, opts)
  p result
rescue Mobwa::ApiError => e
  puts "Error when calling TransfersApi->update_transfer: #{e}"
end
```

#### Using the update_transfer_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(<Transfer>, Integer, Hash)> update_transfer_with_http_info(account_id, transfer_id, opts)

```ruby
begin
  # Change transfer information
  data, status_code, headers = api_instance.update_transfer_with_http_info(account_id, transfer_id, opts)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => <Transfer>
rescue Mobwa::ApiError => e
  puts "Error when calling TransfersApi->update_transfer_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **account_id** | **String** | The id of the account |  |
| **transfer_id** | **String** | The id of the transfers to operate on |  |
| **update_transfer_request** | [**UpdateTransferRequest**](UpdateTransferRequest.md) |  | [optional] |

### Return type

[**Transfer**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

