# Mobwa::SignUpRequest

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **username** | **String** |  | [optional] |
| **email** | **String** |  | [optional] |
| **password** | **String** |  | [optional] |

## Example

```ruby
require 'mobwa'

instance = Mobwa::SignUpRequest.new(
  username: null,
  email: null,
  password: null
)
```

