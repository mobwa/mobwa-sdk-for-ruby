# Mobwa::TransferSource

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **account_id** | **String** |  | [optional] |

## Example

```ruby
require 'mobwa'

instance = Mobwa::TransferSource.new(
  account_id: null
)
```

