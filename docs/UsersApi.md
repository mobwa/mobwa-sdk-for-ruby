# Mobwa::UsersApi

All URIs are relative to *http://localhost*

| Method | HTTP request | Description |
| ------ | ------------ | ----------- |
| [**create_user**](UsersApi.md#create_user) | **POST** /users | Create a user |
| [**get_user**](UsersApi.md#get_user) | **GET** /users/{userId} | Get user information |
| [**list_users**](UsersApi.md#list_users) | **GET** /users | List all users |
| [**update_user**](UsersApi.md#update_user) | **PUT** /users/{userId} | Update user information |


## create_user

> <Array<Transfer>> create_user(opts)

Create a user

### Examples

```ruby
require 'time'
require 'mobwa'
# setup authorization
Mobwa.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Mobwa::UsersApi.new
opts = {
  create_user_request: Mobwa::CreateUserRequest.new # CreateUserRequest | 
}

begin
  # Create a user
  result = api_instance.create_user(opts)
  p result
rescue Mobwa::ApiError => e
  puts "Error when calling UsersApi->create_user: #{e}"
end
```

#### Using the create_user_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(<Array<Transfer>>, Integer, Hash)> create_user_with_http_info(opts)

```ruby
begin
  # Create a user
  data, status_code, headers = api_instance.create_user_with_http_info(opts)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => <Array<Transfer>>
rescue Mobwa::ApiError => e
  puts "Error when calling UsersApi->create_user_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **create_user_request** | [**CreateUserRequest**](CreateUserRequest.md) |  | [optional] |

### Return type

[**Array&lt;Transfer&gt;**](Transfer.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json


## get_user

> <Account> get_user(user_id)

Get user information

### Examples

```ruby
require 'time'
require 'mobwa'
# setup authorization
Mobwa.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Mobwa::UsersApi.new
user_id = 'user_id_example' # String | The id of the user to operate on

begin
  # Get user information
  result = api_instance.get_user(user_id)
  p result
rescue Mobwa::ApiError => e
  puts "Error when calling UsersApi->get_user: #{e}"
end
```

#### Using the get_user_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(<Account>, Integer, Hash)> get_user_with_http_info(user_id)

```ruby
begin
  # Get user information
  data, status_code, headers = api_instance.get_user_with_http_info(user_id)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => <Account>
rescue Mobwa::ApiError => e
  puts "Error when calling UsersApi->get_user_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **user_id** | **String** | The id of the user to operate on |  |

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## list_users

> <Array<User>> list_users

List all users

### Examples

```ruby
require 'time'
require 'mobwa'
# setup authorization
Mobwa.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Mobwa::UsersApi.new

begin
  # List all users
  result = api_instance.list_users
  p result
rescue Mobwa::ApiError => e
  puts "Error when calling UsersApi->list_users: #{e}"
end
```

#### Using the list_users_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(<Array<User>>, Integer, Hash)> list_users_with_http_info

```ruby
begin
  # List all users
  data, status_code, headers = api_instance.list_users_with_http_info
  p status_code # => 2xx
  p headers # => { ... }
  p data # => <Array<User>>
rescue Mobwa::ApiError => e
  puts "Error when calling UsersApi->list_users_with_http_info: #{e}"
end
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**Array&lt;User&gt;**](User.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json


## update_user

> <Account> update_user(user_id, opts)

Update user information

### Examples

```ruby
require 'time'
require 'mobwa'
# setup authorization
Mobwa.configure do |config|
  # Configure HTTP basic authorization: basicAuth
  config.username = 'YOUR USERNAME'
  config.password = 'YOUR PASSWORD'
end

api_instance = Mobwa::UsersApi.new
user_id = 'user_id_example' # String | The id of the user to operate on
opts = {
  update_account_request: Mobwa::UpdateAccountRequest.new # UpdateAccountRequest | 
}

begin
  # Update user information
  result = api_instance.update_user(user_id, opts)
  p result
rescue Mobwa::ApiError => e
  puts "Error when calling UsersApi->update_user: #{e}"
end
```

#### Using the update_user_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(<Account>, Integer, Hash)> update_user_with_http_info(user_id, opts)

```ruby
begin
  # Update user information
  data, status_code, headers = api_instance.update_user_with_http_info(user_id, opts)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => <Account>
rescue Mobwa::ApiError => e
  puts "Error when calling UsersApi->update_user_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **user_id** | **String** | The id of the user to operate on |  |
| **update_account_request** | [**UpdateAccountRequest**](UpdateAccountRequest.md) |  | [optional] |

### Return type

[**Account**](Account.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

