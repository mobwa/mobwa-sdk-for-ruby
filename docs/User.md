# Mobwa::User

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **username** | **String** |  | [optional] |
| **email** | **String** |  | [optional] |
| **role** | **String** |  | [optional] |

## Example

```ruby
require 'mobwa'

instance = Mobwa::User.new(
  username: null,
  email: null,
  role: null
)
```

