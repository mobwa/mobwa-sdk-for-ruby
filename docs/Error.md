# Mobwa::Error

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **code** | **Integer** |  |  |
| **message** | **String** |  |  |

## Example

```ruby
require 'mobwa'

instance = Mobwa::Error.new(
  code: null,
  message: null
)
```

