# Mobwa::SignupApi

All URIs are relative to *http://localhost*

| Method | HTTP request | Description |
| ------ | ------------ | ----------- |
| [**signup**](SignupApi.md#signup) | **POST** /signup | Signup |


## signup

> <SignUpResponse> signup(opts)

Signup

### Examples

```ruby
require 'time'
require 'mobwa'

api_instance = Mobwa::SignupApi.new
opts = {
  sign_up_request: Mobwa::SignUpRequest.new # SignUpRequest | 
}

begin
  # Signup
  result = api_instance.signup(opts)
  p result
rescue Mobwa::ApiError => e
  puts "Error when calling SignupApi->signup: #{e}"
end
```

#### Using the signup_with_http_info variant

This returns an Array which contains the response data, status code and headers.

> <Array(<SignUpResponse>, Integer, Hash)> signup_with_http_info(opts)

```ruby
begin
  # Signup
  data, status_code, headers = api_instance.signup_with_http_info(opts)
  p status_code # => 2xx
  p headers # => { ... }
  p data # => <SignUpResponse>
rescue Mobwa::ApiError => e
  puts "Error when calling SignupApi->signup_with_http_info: #{e}"
end
```

### Parameters

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **sign_up_request** | [**SignUpRequest**](SignUpRequest.md) |  | [optional] |

### Return type

[**SignUpResponse**](SignUpResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

