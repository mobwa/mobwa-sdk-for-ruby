# Mobwa::Account

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **id** | **String** |  | [optional] |
| **name** | **String** |  | [optional] |
| **created_at** | **String** |  | [optional] |
| **currency** | **String** |  | [optional] |
| **updated_at** | **String** |  | [optional] |
| **balance** | **Integer** |  | [optional] |

## Example

```ruby
require 'mobwa'

instance = Mobwa::Account.new(
  id: null,
  name: null,
  created_at: null,
  currency: null,
  updated_at: null,
  balance: null
)
```

