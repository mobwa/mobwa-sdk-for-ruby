# Mobwa::CreateUserRequest

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **username** | **String** |  | [optional] |
| **email** | **String** |  | [optional] |
| **password** | **String** |  | [optional] |
| **role** | **String** |  | [optional] |

## Example

```ruby
require 'mobwa'

instance = Mobwa::CreateUserRequest.new(
  username: null,
  email: null,
  password: null,
  role: null
)
```

