# Mobwa::UpdateTransferRequest

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **message** | **String** |  | [optional] |
| **amount** | **Integer** |  | [optional] |

## Example

```ruby
require 'mobwa'

instance = Mobwa::UpdateTransferRequest.new(
  message: null,
  amount: null
)
```

