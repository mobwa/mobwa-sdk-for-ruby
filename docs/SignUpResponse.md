# Mobwa::SignUpResponse

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **username** | **String** |  | [optional] |
| **email** | **String** |  | [optional] |
| **role** | **String** |  | [optional] |
| **accounts** | [**Array&lt;Account&gt;**](Account.md) |  | [optional] |

## Example

```ruby
require 'mobwa'

instance = Mobwa::SignUpResponse.new(
  username: null,
  email: null,
  role: null,
  accounts: null
)
```

