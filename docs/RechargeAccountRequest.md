# Mobwa::RechargeAccountRequest

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **amout** | **Integer** |  | [optional] |

## Example

```ruby
require 'mobwa'

instance = Mobwa::RechargeAccountRequest.new(
  amout: null
)
```

