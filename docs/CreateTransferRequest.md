# Mobwa::CreateTransferRequest

## Properties

| Name | Type | Description | Notes |
| ---- | ---- | ----------- | ----- |
| **message** | **String** |  | [optional] |
| **amount** | **Integer** |  | [optional] |
| **currency** | **String** |  | [optional] |
| **auto_complete** | **Boolean** |  | [optional] |
| **destination** | **String** |  | [optional] |

## Example

```ruby
require 'mobwa'

instance = Mobwa::CreateTransferRequest.new(
  message: null,
  amount: null,
  currency: null,
  auto_complete: null,
  destination: null
)
```

