=begin
#Mobwa Payments Hub

#Mobwa Payments Hub allows anyone to transfer money between different accounts as well as manage them. It  currently supports the following currencies: United States Dollar (USD) and Singapura Dollar (SGD) 

The version of the OpenAPI document: 1.0.0

Generated by: https://openapi-generator.tech
OpenAPI Generator version: 5.2.1-SNAPSHOT

=end

require 'spec_helper'
require 'json'

# Unit tests for Mobwa::AccountsApi
# Automatically generated by openapi-generator (https://openapi-generator.tech)
# Please update as you see appropriate
describe 'AccountsApi' do
  before do
    # run before each test
    @api_instance = Mobwa::AccountsApi.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of AccountsApi' do
    it 'should create an instance of AccountsApi' do
      expect(@api_instance).to be_instance_of(Mobwa::AccountsApi)
    end
  end

  # unit tests for create_account
  # Create an account
  # @param [Hash] opts the optional parameters
  # @option opts [CreateAccountRequest] :create_account_request 
  # @return [Array<Transfer>]
  describe 'create_account test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for get_account
  # Get account information
  # @param account_id The id of the account
  # @param [Hash] opts the optional parameters
  # @return [Account]
  describe 'get_account test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for list_account
  # List all accounts
  # @param [Hash] opts the optional parameters
  # @return [Array<Account>]
  describe 'list_account test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for recharge_account
  # Recharge the account
  # @param account_id The id of the account
  # @param [Hash] opts the optional parameters
  # @option opts [RechargeAccountRequest] :recharge_account_request 
  # @return [Account]
  describe 'recharge_account test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for update_account
  # Update account information
  # @param account_id The id of the account
  # @param [Hash] opts the optional parameters
  # @option opts [UpdateAccountRequest] :update_account_request 
  # @return [Account]
  describe 'update_account test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  # unit tests for withdraw_money
  # Withdraw money from the account
  # @param account_id The id of the account
  # @param [Hash] opts the optional parameters
  # @option opts [RechargeAccountRequest] :body 
  # @return [Account]
  describe 'withdraw_money test' do
    it 'should work' do
      # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end
